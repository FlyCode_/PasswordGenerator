package cc.flycode.PasswordGenerator;

import cc.flycode.PasswordGenerator.GUI.PasswordGeneratorGUI;
import cc.flycode.PasswordGenerator.Utils.Logger;
import cc.flycode.PasswordGenerator.Utils.UpdateUtils;

import javax.swing.*;

/**
 * Created by FlyCode on 19/07/2018 Package cc.flycode.PasswordGenerator
 */

public class Loader {
    public double version = 0.1;
    public boolean Debug = false;
    public boolean updateNeeded = false;
    public static void main(String[] args) {
        Loader loader = new Loader();
        loader.checkUpdates();
        Logger.log("Starting...",true);
        try {
            Logger.log("Attempting to start GUI..",false);
            loader.loadPanel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void loadPanel() {
        PasswordGeneratorGUI passwordGeneratorGUI = new PasswordGeneratorGUI();
        passwordGeneratorGUI.loadPanel();
    }
    public void checkUpdates() {
        Logger.log("Checking for updates...",true);
        UpdateUtils updateUtils = new UpdateUtils();
        updateNeeded = updateUtils.checkUpdate(version);
        if (updateNeeded) {
            Logger.log("A Update has been found !",true);
            JOptionPane.showMessageDialog(new JFrame(),"A Update has been found:\nNew Version: " + updateUtils.getServerVersion() + "\nYour Version: " + version);
        }
    }
}
