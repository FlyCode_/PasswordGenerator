package cc.flycode.PasswordGenerator.Utils;

import cc.flycode.PasswordGenerator.Loader;

/**
 * Created by FlyCode on 19/07/2018 Package cc.flycode.PasswordGenerator.Utils
 */

public class Logger {
    public static void log(String s,boolean bypassMode) {
        if (s != null) {
            Loader loader = new Loader();
            if (loader.Debug) {
                System.out.println("[RPG] - " + s);
            } else if (bypassMode) {
                System.out.println("[RPG] - " + s);
            }
        }
    }
}
