package cc.flycode.PasswordGenerator.Utils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 * Created by FlyCode on 19/07/2018 Package cc.flycode.PasswordGenerator.Utils
 */

public class SystemUtils {
    public void ctc(String s) {
        StringSelection stringSelection = new StringSelection(s);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
        Logger.log("Copied string to clipboard",false);
    }
}
