package cc.flycode.PasswordGenerator.Utils;

/**
 * Created by FlyCode on 19/07/2018 Package cc.flycode.PasswordGenerator.Utils
 */

public class Rating {
    public Enum rating(int len) {
        if (len < 10) {
            return ratings.POOR;
        } else if (len > 10 && len < 15) {
            return ratings.WEEK;
        } else if (len >= 15 && len < 19) {
            return ratings.GOOD;
        } else if (len >= 19 && len < 25) {
            return ratings.ADVANCED;
        } else if (len >= 25 && len < 50) {
            return ratings.INSANE;
        } else if (len >= 50) {
            return ratings.WTF;
        }
        return ratings.NULl;
    }
    enum ratings {
        POOR,WEEK,GOOD,ADVANCED,INSANE,NULl,WTF
    }
}
