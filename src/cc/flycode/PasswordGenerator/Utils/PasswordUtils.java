package cc.flycode.PasswordGenerator.Utils;

import java.util.Random;

/**
 * Created by FlyCode on 19/07/2018 Package cc.flycode.PasswordGenerator.Utils
 */

public class PasswordUtils {
    public String createRandomPassword(int len) {
        String Capitalchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Smallchars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789"+MathUtils.getRandomInteger(MathUtils.getRandomInteger(1,5),MathUtils.getRandomInteger(10003,99990))*10/2;
        String symbols = "!@#$%^&*_=+-/.?<>)";
        Random random = new Random();
        char[] PasswordOut = new char[len];
        String values = Capitalchars+Smallchars+numbers+symbols;
        for (int i = 0; i < len; i++) {
            PasswordOut[i] = values.charAt(random.nextInt(values.length()));
        }
        return String.valueOf(PasswordOut);
    }
}
