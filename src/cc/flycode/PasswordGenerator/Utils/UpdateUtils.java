package cc.flycode.PasswordGenerator.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

/**
 * Created by FlyCode on 19/07/2018 Package cc.flycode.PasswordGenerator.Utils
 */

public class UpdateUtils {
    private boolean isNeeded = false;
    public boolean checkUpdate(double cv) {
        double parse = Double.parseDouble(getPage());
        if (parse > cv) {
            isNeeded = true;
            return true;
        } else if (parse <= cv) {
            return false;
        }
        return false;
    }
    public double getServerVersion() {
        if (isNeeded) {
            double parse = Double.parseDouble(getPage());
            return parse;
        }
        return 0.0;
    }
    private String getPage() {
        try {
            URLConnection connection = new URL("http://meta.flycode.cc/OpenSRC/RandPasswordGen/version.txt").openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            connection.connect();
            BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                sb.append(line);
            }
            String value = sb.toString();
            return value;
        } catch (IOException ex) {
        }
        return null;
    }
}
