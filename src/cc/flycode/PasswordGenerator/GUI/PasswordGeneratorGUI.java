package cc.flycode.PasswordGenerator.GUI;

import cc.flycode.PasswordGenerator.Loader;
import cc.flycode.PasswordGenerator.Utils.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Created by FlyCode on 19/07/2018 Package cc.flycode.PasswordGenerator.GUI
 */

public class PasswordGeneratorGUI {
    private JPanel MainPanel;
    private JButton genButton;
    private JTextField PasswordOut;
    private JSlider lenSlider;
    private JLabel lengthText;
    private JButton ctcButton;

    public PasswordGeneratorGUI() {
        genButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Logger.log("Creating a random password...",false);
                PasswordUtils passwordUtils = new PasswordUtils();
                String pwd = passwordUtils.createRandomPassword(lenSlider.getValue());
                PasswordOut.setText(pwd);
            }
        });
        lenSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                Rating rating = new Rating();
                lengthText.setText("Length: " + lenSlider.getValue() + " ("+rating.rating(lenSlider.getValue()).name()+")");
            }
        });

        MainPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                Rating rating = new Rating();
                lengthText.setText("Length: 15 ("+rating.rating(15).name()+")");
                super.componentResized(e);
            }
        });
        ctcButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (PasswordOut.getText().length() >= 5) {
                    SystemUtils systemUtils = new SystemUtils();
                    systemUtils.ctc(PasswordOut.getText());
                }
            }
        });
    }

    public void loadPanel() {
        Loader loader = new Loader();
        JFrame frame = new JFrame("Random Password Generator ("+loader.version+")");
        frame.setContentPane(new PasswordGeneratorGUI().MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(790,290);
        frame.setVisible(true);
        frame.setResizable(false);
        lengthText.setText("Length: 15");
        lenSlider.setValue(15);
    }
}
